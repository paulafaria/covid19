import React, { Component } from 'react';
import { HashRouter, Route } from 'react-router-dom';

import { Home, BeloHorizonte } from './pages';

class App extends Component {

  render() {
    return (
      <HashRouter basename='/'>
         <div className="wrapper">
            <Route exact path='/' component={Home}/>
            <Route exact path='/bh' component={BeloHorizonte}/>
         </div>
     </HashRouter>
    );
  } 
}
export default App;
