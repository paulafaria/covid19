import React, { Component } from 'react';
import styled from 'styled-components';


const ResultContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const CardHeader = styled.div`
  height: 120px;
  position: relative;
  display: flex;
  justify-content: center;
  background-position: center top;
  background-size: cover;
`;

const CardImage = styled.div`
  background: white;
  border-radius: 100px;
  width: 80px;
  height: 80px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 80px;
`;

const CardContent = styled.div`
  padding-top: 16px;
`;

const CardTitle = styled.h3`
  color: #666d77;
  text-align: center;
  margin-top: 30px;
`;

const Card = styled.div`
  background-color: white;
  margin: 24px 0;
  width: 300px;
`;

const CardData = styled.div`
  display: flex;
  margin: 30px 0;
`;

const CardDataItem = styled.div`
  width: 50%;
  text-align: center;
`;

const CardDataTitle = styled.div`
    font-size: 11px;
    text-transform: uppercase;
    margin-bottom: 10px;
    color: #666d77;
    font-weight: 600;
`;

const CardDataNumberConfirmed = styled.div`
  color: #e02020;
  font-size: 26px;
`;

const CardDataNumberSuspect = styled.div`
  color: #fcbd00;
  font-size: 26px;
`;

const CardDataNumberDeaths = styled.div`
  margin: 0;
  color: #FFF;
  padding: 8px;
  font-size: 14px;
`;


class Header extends Component {
  constructor() {
    super();
    
    this.state = {
      cases: []
    }
  }

  componentDidMount() {

    fetch(`https://covid19-brazil-api.now.sh/api/report/v1`)
    .then( results => { return results.json();})
    .then(data => {
      this.setState({cases:data.data});
    });

  }

  render () {
    return (
      <div>
        <ResultContainer>
            {
              this.state.cases.map((item) => {
                return (
                  <Card key={item.uid}>
                    <div>
                      <CardHeader style={{backgroundImage: `url(${process.env.PUBLIC_URL}/assets/images/cities/${item.uf}.png)`}}> 
                        <CardImage>
                          <img src={`${process.env.PUBLIC_URL}/assets/images/flags/${item.uf}.svg`} width="50" alt={item.state}/>
                        </CardImage>
                      </CardHeader>
                      <CardContent>
                        <CardTitle>{item.state} - {item.uf}</CardTitle>
                        <CardData>
                          <CardDataItem>
                            <CardDataTitle>
                              confirmados
                            </CardDataTitle>
                            <CardDataNumberConfirmed>
                              {item.cases}
                            </CardDataNumberConfirmed>
                          </CardDataItem>
                          <CardDataItem>
                            <CardDataTitle>
                              suspeitos
                            </CardDataTitle>
                            <CardDataNumberSuspect>
                            {item.suspects}
                            </CardDataNumberSuspect>
                          </CardDataItem>
                        </CardData>
                        {item.deaths > 1 ? 
                          <CardDataNumberDeaths style={{backgroundColor: '#ff7979'}}>
                          <strong>mortes:</strong> {item.deaths}
                          </CardDataNumberDeaths>
                        : ''}
                      </CardContent>
                    </div>
                  </Card>
                )
              })
            }
        </ResultContainer>
      </div>
    )
  }
}

export default Header;